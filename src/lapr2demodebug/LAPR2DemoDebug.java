/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr2demodebug;

/**
 *
 * @author amartins
 */
public class LAPR2DemoDebug {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ComplexNumber a, b, res = new ComplexNumber(0,0);
        
        for( int i=0; i<10; i++){
            a = new ComplexNumber(i,0);
            b = new ComplexNumber(0,i);
            System.out.println(a + " - " + b);
            res = res.add(a.add(b));
        }
        System.out.println(res);
    }
    
}
